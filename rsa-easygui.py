# -*- coding: UTF-8 -*-
#python3

import random
from glob import glob
from easygui import msgbox as msg
from easygui import buttonbox as btn
from easygui import multpasswordbox as passwd
from easygui import enterbox as enter
from easygui import choicebox as ch

def PrimeNumSelector(i):
	A = True
	for a in range(2, i):
		for b in range(2, i):
			if a * b > i:
				break
			if a * b == i:
				A = False
	return A

def PubKeySelector(PubKey, EularFunction):
	A = False
	for b in range(2, EularFunction):
		if ((PubKey > 1) and (PubKey < EularFunction) and (PubKey * b != EularFunction)):
			A = True
	return A

while (True):
	Selection = btn(msg = "请选择你的操作\n(未生成密钥或已删除密钥时请先生成密钥)", title = "RSA-EasyGui-请选择你的操作", choices = ("生成密钥", "加密", "解密", "退出程序"))
	if Selection == "生成密钥":
		PrimeNumList = []
		while True:
			n = int(enter(msg = "请输入一个大于2的数(越大安全性越高)：", title = "RSA-EasyGui-请输入一个大于2的数"))
			i = 2
			while (len(PrimeNumList) <= n): #将输入范围内质数添加到列表
				if (PrimeNumSelector(i) == True):
					PrimeNumList.append(i)
				i += 1
			if len(PrimeNumList) < 2:
				msg(msg = "输入的数字太小，请重新输入。", title = "RSA-EasyGui-ERROR-输入的数字太小...")
			else:
				break
		p = random.choice(PrimeNumList)
		q = random.choice(PrimeNumList)
		while p == q: #使两个质数不相同
			p = random.choice(PrimeNumList)
			q = random.choice(PrimeNumList)
		PrimeProduct = p * q #质数相乘
		EularFunction = (p - 1) * (q - 1) #欧拉公式
		PubKey = 0
		while (PubKey == 0): #从质数列表中选出公钥
			PubKeyTemp = random.choice(PrimeNumList)
			if (PubKeySelector(PubKeyTemp, EularFunction) == True):
				PubKey = PubKeyTemp
		PriKey = 0
		while ((PriKey * PubKey) % EularFunction != 1): #算出私钥
			PriKey += 1
		#将密钥存储到文件
		#注：生成新的密钥会覆盖旧的密钥
		PubKeyFile = open("PubKey", "w")
		PubKeyFile.write(str(PubKey) + "\n" + str(PrimeProduct))
		PubKeyFile.close()
		PriKeyFile = open("PriKey", "w")
		PriKeyFile.write(str(PriKey) + "\n" + str(PrimeProduct))
		PriKeyFile.close()
		msg(msg = "公钥(PubKey)与私钥(PriKey)存储在该程序所在文件夹", title = "RSA-EasyGui-LOG-公钥(PubKey)与...")
	elif Selection == "加密":
		PubKeyFile = open(str(ch(msg = "请选择公钥文件", title = "RSA-EasyGui-请选择公钥文件", choices = glob("*"))), "r") #打开PubKey文件
		PubKey = PubKeyFile.readlines()
		PlainText = str(enter(msg = "请输入明文：", title = "RSA-EasyGui-请输入..."))
		CipherFile = open("CipherText", "w")
		CipherText = str()
		for i in range(len(PlainText)): #算出密文
			CipherTextCache = str((int(ord(PlainText[i])) ** int(PubKey[0])) % int(PubKey[1]))
			CipherText = str(CipherText) + str(CipherTextCache) + "\n"
		CipherFile.write(CipherText)
		CipherFile.close
		PubKeyFile.close()
		msg("密文文件(CipherText)存储在该程序所在文件夹", title = "RSA-EasyGui-LOG-密文文件(CipherText)存储在...")
	elif Selection == "解密":
		PriKeyFile = open(str(ch(msg = "请选择私钥文件", title = "RSA-EasyGui-请选择私钥文件", choices = glob("*"))), "r") #打开PriKey文件
		PriKey = PriKeyFile.readlines()
		CipherFile = open(str(ch(msg = "请选择密文文件", title = "RSA-EasyGui-请选择密文文件", choices = glob("*"))), "r")
		CipherText = CipherFile.readlines()
		PlainText = str()
		for i in CipherText: #算出明文
			PlainTextCache = chr(int(int(i) ** int(PriKey[0]) % int(PriKey[1])))
			PlainText = str(PlainText) + str(PlainTextCache)
		PriKeyFile.close()
		CipherFile.close()
		msg(msg = "明文如下：\n" + str(PlainText), title = "RSA-EasyGui-LOG-明文如下：...")
	elif Selection == "退出程序":
		print("ERROR: 无法识别，正在退出程序...")
		exit()
